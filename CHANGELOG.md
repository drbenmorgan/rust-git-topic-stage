# v4.0.0 (unreleased)

  * Remove methods deprecated in 2.0.0.
  * Remove the `identity` from the `Stager` object. The committer information
    comes from the configuration file provided by the given context.

# v3.0.0

  * Bump `chrono` from `0.3` to `0.4`. This is required for compatibility with
    `serde-1.0`.

# v2.0.0

  * Updated to `git-workarea-2.0.0`.
  * Deprecated `OldTopicRemoval::topic()` and `StagedTopic::topic()` in
    preference for direct access to the member.

# v1.0.0

  * Initial stable release.
