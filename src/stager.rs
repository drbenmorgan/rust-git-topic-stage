// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::{DateTime, Utc};
use crates::git_workarea::{CommitId, Conflict, GitContext, Identity, MergeResult, MergeStatus};

use error::*;

use std::fmt::{self, Debug, Display};

#[derive(Debug, Clone)]
/// A branch for the stager.
///
/// Topics contain additional information so that they may be easily identified and so that the
/// commit messages are useful to humans as well.
pub struct Topic {
    /// The HEAD commit of the topic branch.
    pub commit: CommitId,
    /// The author of the stage request.
    pub author: Identity,
    /// When the stage request occurred.
    pub stamp: DateTime<Utc>,
    /// An ID for the topic.
    pub id: u64,
    /// The name of the topic.
    pub name: String,
    /// The URL of the topic.
    pub url: String,
}

impl Topic {
    /// Create a topic.
    ///
    /// The ID must be unique across all topics.
    pub fn new<N, U>(commit: CommitId, author: Identity, stamp: DateTime<Utc>, id: u64, name: N,
                     url: U)
                     -> Self
        where N: ToString,
              U: ToString,
    {
        Self {
            commit: commit,
            author: author,
            stamp: stamp,
            id: id,
            name: name.to_string(),
            url: url.to_string(),
        }
    }
}

impl Display for Topic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "topic {} at {}, staged by {} at {}",
               self.name,
               self.commit,
               self.author,
               self.stamp)
    }
}

impl PartialEq for Topic {
    fn eq(&self, rhs: &Self) -> bool {
        // The author and stamp time are explicitly not considered since they are not important to
        // the actual topic comparison once on the stage.
        self.commit == rhs.commit && self.id == rhs.id
    }
}

#[derive(Debug, Clone, PartialEq)]
/// A topic branch which should be integrated into the stage.
pub struct CandidateTopic {
    /// The old revision for the topic (if available).
    pub old_id: Option<Topic>,
    /// The new revision for the topic.
    pub new_id: Topic,
}

impl Display for CandidateTopic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(ref old_topic) = self.old_id {
            write!(f, "candidate {}, replacing {}", self.new_id, old_topic)
        } else {
            write!(f, "candidate {}, new", self.new_id)
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
/// A topic branch which has been staged.
pub struct StagedTopic {
    /// The commit where the topic branch has been merged into the staging branch.
    pub merge: CommitId,
    /// The topic branch.
    pub topic: Topic,
}

impl StagedTopic {
    /// The HEAD commit of the topic branch.
    pub fn commit(&self) -> &CommitId {
        &self.topic.commit
    }
}

impl Display for StagedTopic {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "staged {}, via {}", self.topic, self.merge)
    }
}

/// Reasons for which a branch can be unstaged.
pub enum UnstageReason {
    /// Conflicts occurred while merging the topic.
    MergeConflict(Vec<Conflict>),
}

impl Debug for UnstageReason {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            UnstageReason::MergeConflict(ref conflicts) => {
                write!(f, "MergeConflict ( {} conflicts )", conflicts.len())
            },
        }
    }
}

#[derive(Debug)]
/// Reasons an old topic was removed from the stage.
pub enum OldTopicRemoval {
    /// The topic branch has been obsoleted by an update.
    Obsoleted {
        /// The old topic, as staged.
        old_merge: StagedTopic,
        /// The staged topic branch which has replaced the old topic branch.
        replacement: Option<StagedTopic>,
    },
    /// The topic branch has been removed, without replacement, from the stage.
    Removed(StagedTopic),
}

impl OldTopicRemoval {
    /// The topic branch which was removed.
    pub fn topic(&self) -> &Topic {
        match *self {
            OldTopicRemoval::Obsoleted { old_merge: ref ob, .. } => &ob.topic,
            OldTopicRemoval::Removed(ref rb) => &rb.topic,
        }
    }
}

#[derive(Debug)]
/// Results from integrating a topic.
pub enum IntegrationResult {
    /// The topic is successfully staged into the integration branch.
    Staged(StagedTopic),
    /// The topic is kicked out of the integration branch.
    Unstaged(Topic, UnstageReason),
    /// The topic is not mergeable.
    Unmerged(Topic, MergeStatus),
}

impl IntegrationResult {
    /// The topic branch.
    pub fn topic(&self) -> &Topic {
        match *self {
            IntegrationResult::Staged(ref sb) => &sb.topic,
            IntegrationResult::Unstaged(ref u, _) => u,
            IntegrationResult::Unmerged(ref t, _) => t,
        }
    }

    /// Whether the topic branch is currently staged or not.
    pub fn on_stage(&self) -> bool {
        match *self {
            IntegrationResult::Staged(_) => true,
            IntegrationResult::Unstaged(_, _) |
            IntegrationResult::Unmerged(_, _) => false,
        }
    }
}

/// The result of rewinding the stage back to a point where new commits may be applied.
///
/// The order is such that the first pair represents the old state of the staged topics while the
/// last pair represents the topics which need to be re-integrated into the staging branch.
type RewoundStage = (Option<StagedTopic>, Vec<StagedTopic>, Option<Topic>);

#[derive(Debug)]
/// The results of a stage operation.
pub struct StageResult {
    /// The branch which the operation removed from the stage.
    pub old_topic: Option<OldTopicRemoval>,
    /// Results from reintegrating the other staged topics.
    ///
    /// Other topics may need to be merged into the integration result again and may fail to merge
    /// once another topic is removed from the branch.
    pub results: Vec<IntegrationResult>,
}

#[derive(Debug)]
/// A manager for an integration branch.
///
/// This stores the state of a staging branch and the representative topics.
pub struct Stager {
    /// The git context for the stager to work in.
    ctx: GitContext,
    /// The ordered set of topics currently merged into the topic stage.
    topics: Vec<StagedTopic>,
}

/// The summary prefix to use when staging topics.
const STAGE_TOPIC_PREFIX: &str = "Stage topic '";
/// The summary suffix to use when staging topics.
const STAGE_TOPIC_SUFFIX: &str = "'";
/// The trailier to use for the ID of the topic.
const TOPIC_ID_TRAILER: &str = "Topic-id: ";
/// The trailier to use for the URL of the topic.
const TOPIC_URL_TRAILER: &str = "Topic-url: ";

impl Stager {
    /// Create a new stage from the given commit.
    pub fn new(ctx: &GitContext, base: CommitId) -> Self {
        Self {
            ctx: ctx.clone(),
            topics: vec![Self::make_base_staged_topic(base)],
        }
    }

    /// Create a new stage, discovering topics which have been merged into an integration branch.
    ///
    /// This constructor takes a base branch and the name of the stage branch. It queries the given
    /// Git context for the history of the stage branch from the base and constructs its state from
    /// its first parent history.
    ///
    /// If the stage does not exist, it is created with the base commit as its start.
    ///
    /// Fails if the stage branch history does not appear to be a proper stage branch. A proper
    /// stage branch's first parent history from the base consists only of merge commits with
    /// exactly two parents with required information in its commit message.
    pub fn from_branch(ctx: &GitContext, base: CommitId, stage: CommitId)
                       -> Result<Self> {
        let cat_file = ctx.git()
            .arg("cat-file")
            .arg("-t")
            .arg(stage.as_str())
            .output()
            .chain_err(|| "failed to construct cat-file command")?;
        if cat_file.status.success() {
            let stage_type = String::from_utf8_lossy(&cat_file.stdout);
            if stage_type.trim() != "commit" {
                bail!(ErrorKind::InvalidIntegrationBranch(stage, InvalidCommitReason::NotACommit));
            }
        } else {
            let update_ref = ctx.git()
                .arg("update-ref")
                .arg(stage.as_str())
                .arg(base.as_str())
                .output()
                .chain_err(|| "failed to construct update-ref command")?;
            if !update_ref.status.success() {
                bail!(ErrorKind::Git(format!("failed to update the {} ref to {}: {}",
                                             stage,
                                             base,
                                             String::from_utf8_lossy(&update_ref.stderr))));
            }
        }

        let is_ancestor = ctx.git()
            .arg("merge-base")
            .arg("--is-ancestor")
            .arg(base.as_str())
            .arg(stage.as_str())
            .status()
            .chain_err(|| "failed to construct merge-base command")?;
        let needs_base_update = !is_ancestor.success();

        debug!(target: "git-topic-stage", "creating a stage branch from {} -> {}", base, stage);

        let rev_list = ctx.git()
            .arg("rev-list")
            .arg("--first-parent")
            .arg("--reverse")
            .arg("--parents")
            .arg(stage.as_str())
            .arg(&format!("^{}", base))
            .output()
            .chain_err(|| "failed to construct rev-list command")?;
        if !rev_list.status.success() {
            bail!(ErrorKind::Git(format!("failed to list the first parent history of the stage \
                                          branch: {}",
                                         String::from_utf8_lossy(&rev_list.stderr))));
        }
        let merges = String::from_utf8_lossy(&rev_list.stdout);

        let merge_base = ctx.git()
            .arg("merge-base")
            .arg(base.as_str())
            .arg(stage.as_str())
            .output()
            .chain_err(|| "failed to construct merge-base command")?;
        if !merge_base.status.success() {
            bail!(ErrorKind::InvalidIntegrationBranch(stage, InvalidCommitReason::NotRelated));
        }
        let merge_base = String::from_utf8_lossy(&merge_base.stdout);

        let mut topics = vec![Self::make_base_staged_topic(CommitId::new(merge_base.trim()))];

        let new_topics = merges.lines()
            .map(|merge| {
                let revs = merge.split_whitespace().collect::<Vec<_>>();
                let (rev, parents) = revs[..].split_first().expect("invalid rev-list format");
                let rev_commit = CommitId::new(rev);

                if parents.len() == 1 {
                    bail!(ErrorKind::InvalidIntegrationBranch(rev_commit,
                                                              InvalidCommitReason::NonMergeCommit));
                }

                if parents.len() > 2 {
                    bail!(ErrorKind::InvalidIntegrationBranch(rev_commit,
                                                              InvalidCommitReason::OctopusMerge));
                }

                let commit_info = ctx.git()
                    .arg("log")
                    .arg("--max-count=1")
                    .arg("--pretty=%an%n%ae%n%aI%n%B")
                    .arg(rev)
                    .output()
                    .chain_err(|| "failed to construct log command")?;
                if !commit_info.status.success() {
                    bail!(ErrorKind::Git(format!("failed to get information about a merge on \
                                                  the topic stage: {}",
                                                 String::from_utf8_lossy(&commit_info.stderr))));
                }
                let info = String::from_utf8_lossy(&commit_info.stdout);
                let info = info.lines().collect::<Vec<_>>();

                assert!(info.len() > 6,
                        "expected at least 6 lines of information from the merge message, got {}",
                        info.len());

                let subject = info[3];
                if !subject.starts_with(STAGE_TOPIC_PREFIX) ||
                   !subject.ends_with(STAGE_TOPIC_SUFFIX) {
                    let reason = InvalidCommitReason::InvalidSubject(subject.to_string());
                    bail!(ErrorKind::InvalidIntegrationBranch(rev_commit, reason));
                }
                let name = &subject[STAGE_TOPIC_PREFIX.len()..
                                    subject.len() - STAGE_TOPIC_SUFFIX.len()];

                let mut id = None;
                let mut url = None;

                for line in info[5..info.len() - 1].iter().rev() {
                    if line.is_empty() {
                        break;
                    } else if line.starts_with(TOPIC_ID_TRAILER) {
                        id = Some(line[TOPIC_ID_TRAILER.len()..].parse()
                            .chain_err(|| ErrorKind::IdParse)?);
                    } else if line.starts_with(TOPIC_URL_TRAILER) {
                        url = Some(line[TOPIC_URL_TRAILER.len()..].to_string());
                    } else {
                        warn!(target: "git-topic-stage",
                              "unrecognized trailier in {}: {}",
                              parents[1],
                              line);
                    }
                }

                let url = if let Some(url) = url {
                    url
                } else {
                    bail!(ErrorKind::InvalidIntegrationBranch(rev_commit,
                                                              InvalidCommitReason::MissingUrl));
                };

                let id = match id {
                    Some(0) => {
                        bail!(ErrorKind::InvalidIntegrationBranch(rev_commit,
                                                                  InvalidCommitReason::ZeroId));
                    },
                    Some(id) => id,
                    None => {
                        bail!(ErrorKind::InvalidIntegrationBranch(rev_commit,
                                                                  InvalidCommitReason::MissingId));
                    },
                };

                info!(target: "git-topic-stage",
                      "found staged topic '{}' from {}",
                      name, url);

                Ok(StagedTopic {
                    merge: rev_commit,
                    topic: Topic::new(CommitId::new(parents[1]),
                                      Identity::new(info[0], info[1]),
                                      info[2].parse().chain_err(|| ErrorKind::DateParse)?,
                                      id,
                                      name,
                                      url),
                })
            })
            .collect::<Result<Vec<_>>>()?;

        topics.extend(new_topics.into_iter());

        let mut stager = Self {
            ctx: ctx.clone(),
            topics: topics,
        };

        if needs_base_update {
            let base_update = CandidateTopic {
                old_id: Some(Self::make_base_topic(CommitId::new(merge_base.trim()))),
                new_id: Self::make_base_topic(base),
            };

            stager.stage(base_update)?;
        }

        Ok(stager)
    }

    /// Returns the git context the stager uses for operations.
    pub fn git_context(&self) -> &GitContext {
        &self.ctx
    }

    /// Returns the base branch for the integration branch.
    pub fn base(&self) -> &CommitId {
        self.topics[0].commit()
    }

    /// The topics which have been merged into the stage.
    pub fn topics(&self) -> &[StagedTopic] {
        &self.topics[1..]
    }

    /// The a topic on the stage by its ID.
    pub fn find_topic_by_id(&self, id: u64) -> Option<&StagedTopic> {
        self.topics()
            .iter()
            .find(|staged_topic| id == staged_topic.topic.id)
    }

    /// Find where a topic has been merged into the integration branch.
    pub fn find_topic(&self, topic: &Topic) -> Option<&StagedTopic> {
        self.topics()
            .iter()
            .find(|staged_topic| topic == &staged_topic.topic)
    }

    /// Returns the newest commit in the integration branch.
    pub fn head(&self) -> &CommitId {
        &self.topics.iter().last().expect("expected there to be a HEAD topic on the stage").merge
    }

    /// Make a new staged topic structure from a commit, assuming it is the base of the stage.
    fn make_base_staged_topic(base: CommitId) -> StagedTopic {
        StagedTopic {
            merge: base.clone(),
            topic: Self::make_base_topic(base),
        }
    }

    /// Make a topic structure from a commit, assuming it is the base of the stage.
    fn make_base_topic(base: CommitId) -> Topic {
        Topic {
            commit: base,
            author: Identity::new("stager", "stager@example.com"),
            stamp: Utc::now(),
            id: 0,
            name: "base".to_string(),
            url: "url".to_string(),
        }
    }

    /// Rewind a stage to the topic right before a topic was merged.
    fn rewind_stage(&mut self, topic: CandidateTopic) -> RewoundStage {
        // Find the topic branch's old commit in the current stage.
        let root = topic.old_id
            .and_then(|old| self.topics.iter().position(|staged_topic| old == staged_topic.topic))
            .unwrap_or_else(|| self.topics.len());

        // Grab the branches which must be restaged.
        let mut old_stage = self.topics.drain(root..).collect::<Vec<_>>();

        if self.topics.is_empty() {
            debug!(target: "git-topic-stage", "rewinding the stage to its base");
        } else {
            debug!(target: "git-topic-stage", "rewinding the stage to {}", self.head());
        }

        let (old_topic, new_topic) = if self.topics.is_empty() {
            // TODO: Check that the new base is newer than the old base?

            // The base is being replaced.
            self.topics.push(Self::make_base_staged_topic(topic.new_id.commit));

            // The old topic branch is the first one in the list of branches that need to be
            // restaged, so remove it from the vector.
            (Some(old_stage.remove(0)), None)
        } else if old_stage.is_empty() {
            // This topic is brand new.
            (None, Some(topic.new_id))
        } else {
            // The old topic branch is the first one in the list of branches that need to be
            // restaged, so remove it from the vector.
            (Some(old_stage.remove(0)), Some(topic.new_id))
        };

        (old_topic, old_stage, new_topic)
    }

    /// Replay a set of topics onto the stage.
    fn replay_stage(&mut self, old_stage: Vec<StagedTopic>) -> Result<Vec<IntegrationResult>> {
        debug!(target: "git-topic-stage", "replaying {} branches into the stage", old_stage.len());

        // Restage old branches.
        old_stage.into_iter()
            .map(|old_staged_topic| {
                let (staged, res) = self.merge_to_stage(old_staged_topic.topic)?;
                staged.map(|t| self.topics.push(t));
                Ok(res)
            })
            .collect()
    }

    /// Remove a topic from the stage.
    pub fn unstage(&mut self, topic: StagedTopic) -> Result<StageResult> {
        info!(target: "git-topic-stage", "unstaging a topic: {}", topic);

        if topic.commit() == self.base() {
            debug!(target: "git-topic-stage", "ignoring a request to unstage the base");

            bail!(ErrorKind::CannotUnstageBase);
        }

        let (old_topic, old_stage, new_topic) = self.rewind_stage(CandidateTopic {
            old_id: Some(topic.topic.clone()),
            new_id: topic.topic,
        });

        let results = self.replay_stage(old_stage)?;

        if new_topic.is_none() {
            // Requested the unstaging of the base branch.
            warn!(target: "git-topic-stage", "unstage called on the base branch");
        }

        if let Some(ref old_topic) = old_topic {
            debug!(target: "git-topic-stage", "unstaged {}", old_topic);
        }

        // Give some information about the old topic branch (if it existed).
        let old_branch_result = old_topic.map(OldTopicRemoval::Removed);

        Ok(StageResult {
            old_topic: old_branch_result,
            results: results,
        })
    }

    /// Add a topic branch to the stage.
    ///
    /// If the branch already existed on the staging branch, it is first removed from the stage and
    /// then any topics which were merged after it are merged again, in order. The updated topic is
    /// then merged as the last operation.
    pub fn stage(&mut self, topic: CandidateTopic) -> Result<StageResult> {
        info!(target: "git-topic-stage", "staging a topic: {}", topic);

        // Rewind the stage.
        let (old_topic, old_stage, new_topic) = self.rewind_stage(topic);

        let mut results = self.replay_stage(old_stage)?;

        let old_branch_result = if let Some(topic) = new_topic {
            // Apply the new topic branch.
            let (staged, res) = self.merge_to_stage(topic)?;

            staged.clone().map(|sb| self.topics.push(sb));
            results.push(res);

            // Give some information about the old topic branch (if it existed).
            old_topic.map(|topic| {
                OldTopicRemoval::Obsoleted {
                    old_merge: topic,
                    replacement: staged,
                }
            })
        } else {
            // The base of the branch was replaced; return the old stage base.
            old_topic.map(|topic| {
                OldTopicRemoval::Obsoleted {
                    old_merge: topic,
                    replacement: Some(self.topics[0].clone()),
                }
            })
        };

        Ok(StageResult {
            old_topic: old_branch_result,
            results: results,
        })
    }

    /// Remove all staged topics from the staging branch.
    ///
    /// Previously staged topics are returned.
    pub fn clear(&mut self) -> Vec<StagedTopic> {
        let new_id = Self::make_base_staged_topic(self.base().clone()).topic;
        let old_id = Some(new_id.clone());

        info!(target: "git-topic-stage", "clearing the stage");

        self.rewind_stage(CandidateTopic {
                old_id: old_id,
                new_id: new_id,
            })
            .1
    }

    /// Merge a topic into the stage.
    fn merge_to_stage(&self, topic: Topic) -> Result<(Option<StagedTopic>, IntegrationResult)> {
        let base_commit = self.base();
        let head_commit = self.head();
        let topic_commit = topic.commit.clone();

        debug!(target: "git-topic-stage", "merging {} into the stage", topic);

        // Check that the topic is mergeable against the *base* commit. New root commits may have
        // appeared in topic branches that have since been merged. Checking against the base commit
        // ensures that it at least has a chance if it becomes the first branch on the stage again.
        let merge_status = self.ctx.mergeable(base_commit, &topic_commit)?;
        let bases = if let MergeStatus::Mergeable(bases) = merge_status {
            bases
        } else {
            // Reject branches which are not mergeable.
            debug!(target: "git-topic-stage", "rejecting: unmergeable: {}", merge_status);
            return Ok((None, IntegrationResult::Unmerged(topic.clone(), merge_status)));
        };

        // Prepare a workarea in which to perform the merge.
        let workarea = self.ctx.prepare(head_commit)?;

        // Prepare the merged tree.
        let merge_result = workarea.setup_merge(&bases, head_commit, &topic_commit)?;

        let mut merge_command = match merge_result {
            MergeResult::Conflict(conflicts) => {
                debug!(target: "git-topic-stage", "rejecting: conflicts: {}", conflicts.len());

                return Ok((None,
                           IntegrationResult::Unstaged(topic,
                                                       UnstageReason::MergeConflict(conflicts))));
            },
            MergeResult::Ready(command) => command,
            MergeResult::_Phantom(..) => unreachable!(),
        };

        merge_command.author(&topic.author)
            .author_date(&topic.stamp);

        let merge_commit = merge_command.commit(self.create_message(self.base(), &topic))?;
        let staged_topic = StagedTopic {
            merge: merge_commit,
            topic: topic,
        };

        debug!(target: "git-topic-stage", "successfully staged as {}", staged_topic);

        Ok((Some(staged_topic.clone()), IntegrationResult::Staged(staged_topic)))
    }

    /// Create the merge commit message for a topic.
    fn create_message(&self, _: &CommitId, topic: &Topic) -> String {
        format!("{}{}{}\n\n{}{}\n{}{}\n",
                STAGE_TOPIC_PREFIX,
                topic.name,
                STAGE_TOPIC_SUFFIX,
                TOPIC_ID_TRAILER,
                topic.id,
                TOPIC_URL_TRAILER,
                topic.url)
    }
}
