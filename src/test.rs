// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::{TimeZone, Utc};
use crates::git_workarea::{CommitId, GitContext, Identity};
use crates::log::{self, Log, LogLevel, LogLevelFilter, LogMetadata, LogRecord};

use error::*;
use stager::{CandidateTopic, IntegrationResult, OldTopicRemoval, StagedTopic, Stager, Topic,
             UnstageReason};

use std::path::Path;

fn setup_logging() {
    struct SimpleLogger;

    impl Log for SimpleLogger {
        fn enabled(&self, metadata: &LogMetadata) -> bool {
            metadata.level() <= LogLevel::Debug
        }

        fn log(&self, record: &LogRecord) {
            if self.enabled(record.metadata()) {
                println!("[{}] {}", record.level(), record.args());
            }
        }
    }

    // Since the tests run in parallel, this may get called multiple times. Just ignore errors.
    let _ = log::set_logger(|max_level| {
        max_level.set(LogLevelFilter::Debug);
        Box::new(SimpleLogger)
    });
}

fn git_context() -> GitContext {
    setup_logging();

    let gitdir = concat!(env!("CARGO_MANIFEST_DIR"), "/.git");
    let config = concat!(env!("CARGO_MANIFEST_DIR"), "/test/config");
    GitContext::new_with_config(gitdir, config)
}

const BASE: &str = "3eee1bbaea3e4df5d23fa3731675568fc12c4429";
const STAGE: &str = "8d9ea0c2afb796d2439a4880344c1fa5f8925f8a";
const ADD_CONTENT: &str = "6c688206b267a2bc85de9797190c087f2419858f";
const ADD_CONTENT_MERGE: &str = "700429914a51d2629fb2658d109009f879b958fd";
const ADD_MORE_CONTENT: &str = "205d74e638808e411941193b24ae5cc472728cb6";
const ADD_MORE_CONTENT_MERGE: &str = "8d9ea0c2afb796d2439a4880344c1fa5f8925f8a";
const BLOB_HASH: &str = "1e7caa9ea89a3016bc73f03b5e4c167711a21374";

const STAGE_BAD_MESSAGE_SUBJECT: &str = "eb7d39b3dc3f7d3afb07856c80577a46f78de45a";
const STAGE_BAD_MESSAGE_NO_URL: &str = "7a1b0581e416e0d10ceebce2bcc5652611d243f8";
const STAGE_BAD_MESSAGE_NO_ID: &str = "0c40c51db76f3e05dbe3cf6d4eef4e16a94f0114";
const STAGE_ZERO_ID: &str = "ef1c82d798e8591ff2d93e4476042902620a8570";
const STAGE_OCTOPUS: &str = "1ec897890fad0503ecf8dd5b383911bc85a42727";

const BASE_UPDATE: &str = "12d5e4eb5e85ced667bf384c5ef33098786894da";
const BASE_CONFLICT: &str = "b012aa62a489702c42e223ae7d05cd26db0edceb";
const ADD_CONTENT_UPDATE: &str = "912389b87aab2908359194669f9c27cf20c2ceab";
const ADD_CONTENT_REWRITE: &str = "6073dcbb13125cd9d613379c9d3c945febb02e12";
const ADD_CONTENT_CONFLICT: &str = "fe73844a5e0c816171679c60b5e8b82d934a7571";

fn stager_identity() -> Identity {
    Identity::new("Git Topic Stage Test", "git-topic-stage@example.com")
}

fn topic_identity() -> Identity {
    Identity::new("Ben Boeckel", "ben.boeckel@kitware.com")
}

fn new_topic(commit: &str, identity: &Identity, id: u64, name: &str, url: &str) -> Topic {
    Topic::new(CommitId::new(commit),
               identity.clone(),
               Utc::now(),
               id,
               name,
               url)
}

#[test]
fn test_base_branch() {
    let ctx = git_context();
    let base = CommitId::new(BASE);

    let stager = Stager::new(&ctx, base.clone());

    assert_eq!(stager.base(), &base);
    assert_eq!(stager.topics().len(), 0);
    assert_eq!(stager.head(), &base);
}

#[test]
fn test_stage_branch_empty() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(BASE);

    let stager = Stager::from_branch(&ctx, base.clone(), stage.clone()).unwrap();

    assert_eq!(stager.base(), &base);
    assert_eq!(stager.topics().len(), 0);
    assert_eq!(stager.head(), &base);
}

#[test]
fn test_stage_branch_non_commit() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(BLOB_HASH);

    let err = Stager::from_branch(&ctx, base.clone(), stage.clone())
        .unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason, InvalidCommitReason::NotACommit);
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_no_exist() {
    let ctx = git_context();

    let stage_ref = "refs/stage/doesntexist";
    ctx.git()
        .arg("update-ref")
        .arg("-d")
        .arg(stage_ref)
        .status()
        .unwrap();

    let base = CommitId::new(BASE);
    let stage = CommitId::new(stage_ref);

    let stager = Stager::from_branch(&ctx, base.clone(), stage.clone()).unwrap();

    assert_eq!(stager.base(), &base);
    assert_eq!(stager.topics().len(), 0);
    assert_eq!(stager.head(), &base);
}

#[test]
fn test_stage_branch() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);

    let stager = Stager::from_branch(&ctx, base.clone(), stage.clone()).unwrap();

    assert_eq!(stager.base(), &base);
    assert_eq!(stager.topics().len(), 2);
    assert_eq!(stager.head(), &stage);

    let topics = stager.topics();

    assert_eq!(topics[0],
               StagedTopic {
                   merge: CommitId::new(ADD_CONTENT_MERGE),
                   topic: new_topic(ADD_CONTENT,
                                    &topic_identity(),
                                    1,
                                    "add-content",
                                    "add-content"),
               });
    assert_eq!(topics[0].topic.author, topic_identity());
    assert_eq!(topics[0].topic.stamp,
               Utc.ymd(2016, 08, 01).and_hms(21, 28, 10));
    assert_eq!(topics[1],
               StagedTopic {
                   merge: CommitId::new(ADD_MORE_CONTENT_MERGE),
                   topic: new_topic(ADD_MORE_CONTENT,
                                    &topic_identity(),
                                    2,
                                    "add-more-content",
                                    "add-more-content"),
               });
    assert_eq!(topics[1].topic.author, topic_identity());
    assert_eq!(topics[1].topic.stamp,
               Utc.ymd(2016, 08, 01).and_hms(21, 28, 39));
}

#[test]
fn test_stage_find_topic() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);

    let stager = Stager::from_branch(&ctx, base, stage).unwrap();

    assert_eq!(stager.find_topic(&new_topic(ADD_CONTENT,
                                            &topic_identity(),
                                            1,
                                            "add-content",
                                            "add-content"))
                   .unwrap(),
               &StagedTopic {
                   merge: CommitId::new(ADD_CONTENT_MERGE),
                   topic: new_topic(ADD_CONTENT,
                                    &topic_identity(),
                                    1,
                                    "add-content",
                                    "add-content"),
               });
    assert_eq!(stager.find_topic_by_id(1).unwrap(),
               &StagedTopic {
                   merge: CommitId::new(ADD_CONTENT_MERGE),
                   topic: new_topic(ADD_CONTENT,
                                    &topic_identity(),
                                    1,
                                    "add-content",
                                    "add-content"),
               });
    assert_eq!(stager.find_topic(&new_topic(ADD_MORE_CONTENT,
                                            &topic_identity(),
                                            2,
                                            "add-more-content",
                                            "add-more-content"))
                   .unwrap(),
               &StagedTopic {
                   merge: CommitId::new(ADD_MORE_CONTENT_MERGE),
                   topic: new_topic(ADD_MORE_CONTENT,
                                    &topic_identity(),
                                    2,
                                    "add-more-content",
                                    "add-more-content"),
               });
    assert_eq!(stager.find_topic_by_id(2).unwrap(),
               &StagedTopic {
                   merge: CommitId::new(ADD_MORE_CONTENT_MERGE),
                   topic: new_topic(ADD_MORE_CONTENT,
                                    &topic_identity(),
                                    2,
                                    "add-more-content",
                                    "add-more-content"),
               });
    assert_eq!(stager.find_topic(&new_topic(BASE, &stager_identity(), 0, "base", "base")),
               None);
    assert_eq!(stager.find_topic(&new_topic(ADD_CONTENT_CONFLICT,
                                            &topic_identity(),
                                            1,
                                            "add-content-conflict",
                                            "add-content-conflict")),
               None);
    assert_eq!(stager.find_topic(&new_topic(ADD_CONTENT_MERGE,
                                            &topic_identity(),
                                            1,
                                            "add-content-merge",
                                            "add-content-merge")),
               None);
    assert_eq!(stager.find_topic_by_id(0), None);
}

#[test]
fn test_stage_branch_invalid_non_merge() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(ADD_CONTENT);

    let err = Stager::from_branch(&ctx, base, stage.clone()).unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason, InvalidCommitReason::NonMergeCommit);
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_invalid_merge_message_subject() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE_BAD_MESSAGE_SUBJECT);

    let err = Stager::from_branch(&ctx, base, stage.clone()).unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason,
                       InvalidCommitReason::InvalidSubject("Merge branch \
                                                            'tests/topic/add-content' into \
                                                            tests/base"
                           .to_string()));
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_invalid_merge_message_no_url() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE_BAD_MESSAGE_NO_URL);

    let err = Stager::from_branch(&ctx, base, stage.clone()).unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason, InvalidCommitReason::MissingUrl);
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_invalid_merge_message_no_id() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE_BAD_MESSAGE_NO_ID);

    let err = Stager::from_branch(&ctx, base, stage.clone()).unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason, InvalidCommitReason::MissingId);
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_invalid_merge_id() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE_ZERO_ID);

    let err = Stager::from_branch(&ctx, base, stage.clone()).unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason, InvalidCommitReason::ZeroId);
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_invalid_octopus_merge() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE_OCTOPUS);

    let err = Stager::from_branch(&ctx, base, stage.clone()).unwrap_err();

    match *err.kind() {
        ErrorKind::InvalidIntegrationBranch(ref ci, ref reason) => {
            assert_eq!(*ci, stage);
            assert_eq!(*reason, InvalidCommitReason::OctopusMerge);
        },
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_branch_stale_stage() {
    let ctx = git_context();
    let base = CommitId::new(ADD_MORE_CONTENT);
    let stage = CommitId::new(ADD_CONTENT_MERGE);

    let stager = Stager::from_branch(&ctx, base, stage.clone()).unwrap();

    assert_eq!(stager.find_topic(&new_topic(ADD_CONTENT,
                                            &topic_identity(),
                                            1,
                                            "add-content",
                                            "add-content"))
                   .unwrap(),
               &StagedTopic {
                   merge: stager.head().clone(),
                   topic: new_topic(ADD_CONTENT,
                                    &topic_identity(),
                                    1,
                                    "add-content",
                                    "add-content"),
               });
    assert_eq!(stager.find_topic_by_id(1).unwrap(),
               &StagedTopic {
                   merge: stager.head().clone(),
                   topic: new_topic(ADD_CONTENT,
                                    &topic_identity(),
                                    1,
                                    "add-content",
                                    "add-content"),
               });
}

#[test]
fn test_stage_unstage_tip() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.unstage(StagedTopic {
            merge: CommitId::new(ADD_MORE_CONTENT_MERGE),
            topic: new_topic(ADD_MORE_CONTENT,
                             &topic_identity(),
                             2,
                             "add-more-content",
                             "add-more-content"),
        })
        .unwrap();

    assert_eq!(result.results.len(), 0);

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Removed(ref st) => {
            assert_eq!(st.merge, CommitId::new(ADD_MORE_CONTENT_MERGE));
            assert_eq!(st.topic,
                       new_topic(ADD_MORE_CONTENT,
                                 &topic_identity(),
                                 2,
                                 "add-more-content",
                                 "add-more-content"));
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}

#[test]
fn test_stage_unstage_mid() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.unstage(StagedTopic {
            merge: CommitId::new(ADD_CONTENT_MERGE),
            topic: new_topic(ADD_CONTENT,
                             &topic_identity(),
                             1,
                             "add-content",
                             "add-content"),
        })
        .unwrap();

    assert_eq!(result.results.len(), 1);

    let iresult = &result.results[0];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_MORE_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Removed(ref st) => {
            assert_eq!(st.merge, CommitId::new(ADD_CONTENT_MERGE));
            assert_eq!(st.topic,
                       new_topic(ADD_CONTENT,
                                 &topic_identity(),
                                 1,
                                 "add-content",
                                 "add-content"));
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}

#[test]
fn test_stage_unstage_base() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.unstage(StagedTopic {
        merge: CommitId::new(BASE),
        topic: new_topic(BASE, &topic_identity(), 0, "base", "base"),
    });
    assert!(result.is_err());

    let err = result.err().unwrap();

    match *err.kind() {
        ErrorKind::CannotUnstageBase => (),
        _ => panic!("unexpected error: {:?}", err),
    }
}

#[test]
fn test_stage_stage_base_update() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);
    let candidate = CandidateTopic {
        old_id: Some(new_topic(BASE, &stager_identity(), 0, "base", "base")),
        new_id: new_topic(BASE_UPDATE, &topic_identity(), 0, "base", "base"),
    };

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.stage(candidate).unwrap();

    assert_eq!(result.results.len(), 2);

    let iresult = &result.results[0];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    let iresult = &result.results[1];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_MORE_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Obsoleted { ref old_merge, ref replacement } => {
            assert_eq!(old_merge,
                       &StagedTopic {
                           merge: CommitId::new(BASE),
                           topic: new_topic(BASE, &stager_identity(), 0, "base", "base"),
                       });
            let replacement = replacement.as_ref().unwrap();
            assert_eq!(replacement.commit(), &CommitId::new(BASE_UPDATE));
            assert_eq!(&replacement.merge, &CommitId::new(BASE_UPDATE));
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}

#[test]
fn test_stage_stage_base_conflict() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);
    let candidate = CandidateTopic {
        old_id: Some(new_topic(BASE, &stager_identity(), 0, "base", "base")),
        new_id: new_topic(BASE_CONFLICT, &stager_identity(), 0, "base", "base"),
    };

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.stage(candidate).unwrap();

    assert_eq!(result.results.len(), 2);

    let iresult = &result.results[0];
    match *iresult {
        IntegrationResult::Unstaged(ref t, ref reason) => {
            assert_eq!(t.commit, CommitId::new(ADD_CONTENT));

            match *reason {
                UnstageReason::MergeConflict(ref cs) => {
                    assert_eq!(cs.len(), 2);
                    assert_eq!(cs[0].path(), Path::new("content"));
                    assert_eq!(cs[1].path(), Path::new("content"));
                },
                //_ => panic!("invalid unstaging reason: {:?}", reason),
            }
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    let iresult = &result.results[1];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_MORE_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Obsoleted { ref old_merge, ref replacement } => {
            assert_eq!(old_merge,
                       &StagedTopic {
                           merge: CommitId::new(BASE),
                           topic: new_topic(BASE, &stager_identity(), 0, "base", "base"),
                       });
            let replacement = replacement.as_ref().unwrap();
            assert_eq!(replacement.commit(), &CommitId::new(BASE_CONFLICT));
            assert_eq!(&replacement.merge, &CommitId::new(BASE_CONFLICT));
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}

#[test]
fn test_stage_stage_mid_update() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);
    let candidate = CandidateTopic {
        old_id: Some(new_topic(ADD_CONTENT,
                               &topic_identity(),
                               1,
                               "add-content",
                               "add-content")),
        new_id: new_topic(ADD_CONTENT_UPDATE,
                          &topic_identity(),
                          1,
                          "add-content",
                          "add-content"),
    };

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.stage(candidate).unwrap();

    assert_eq!(result.results.len(), 2);

    let iresult = &result.results[0];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_MORE_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    let iresult = &result.results[1];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_CONTENT_UPDATE));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Obsoleted { ref old_merge, ref replacement } => {
            assert_eq!(old_merge,
                       &StagedTopic {
                           merge: CommitId::new(ADD_CONTENT_MERGE),
                           topic: new_topic(ADD_CONTENT,
                                            &topic_identity(),
                                            1,
                                            "add-content",
                                            "add-content"),
                       });
            let replacement = replacement.as_ref().unwrap();
            assert_eq!(replacement.commit(), &CommitId::new(ADD_CONTENT_UPDATE));
            assert_eq!(replacement.merge.as_str().len(), 40);
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}

#[test]
fn test_stage_stage_mid_rewrite() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);
    let candidate = CandidateTopic {
        old_id: Some(new_topic(ADD_CONTENT,
                               &topic_identity(),
                               1,
                               "add-content",
                               "add-content")),
        new_id: new_topic(ADD_CONTENT_REWRITE,
                          &topic_identity(),
                          1,
                          "add-content",
                          "add-content"),
    };

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.stage(candidate).unwrap();

    assert_eq!(result.results.len(), 2);

    let iresult = &result.results[0];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_MORE_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    let iresult = &result.results[1];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_CONTENT_REWRITE));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Obsoleted { ref old_merge, ref replacement } => {
            assert_eq!(old_merge,
                       &StagedTopic {
                           merge: CommitId::new(ADD_CONTENT_MERGE),
                           topic: new_topic(ADD_CONTENT,
                                            &topic_identity(),
                                            1,
                                            "add-content",
                                            "add-content"),
                       });
            let replacement = replacement.as_ref().unwrap();
            assert_eq!(replacement.commit(), &CommitId::new(ADD_CONTENT_REWRITE));
            assert_eq!(replacement.merge.as_str().len(), 40);
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}

#[test]
fn test_stage_stage_mid_conflict() {
    let ctx = git_context();
    let base = CommitId::new(BASE);
    let stage = CommitId::new(STAGE);
    let candidate = CandidateTopic {
        old_id: Some(new_topic(ADD_CONTENT,
                               &topic_identity(),
                               1,
                               "add-content",
                               "add-content")),
        new_id: new_topic(ADD_CONTENT_CONFLICT,
                          &topic_identity(),
                          1,
                          "add-content",
                          "add-content"),
    };

    let mut stager = Stager::from_branch(&ctx, base, stage).unwrap();
    let result = stager.stage(candidate).unwrap();

    assert_eq!(result.results.len(), 2);

    let iresult = &result.results[0];
    match *iresult {
        IntegrationResult::Staged(ref st) => {
            assert_eq!(st.commit(), &CommitId::new(ADD_MORE_CONTENT));
            assert_eq!(st.merge.as_str().len(), 40);
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    let iresult = &result.results[1];
    match *iresult {
        IntegrationResult::Unstaged(ref t, ref reason) => {
            assert_eq!(&t.commit, &CommitId::new(ADD_CONTENT_CONFLICT));

            match *reason {
                UnstageReason::MergeConflict(ref cs) => {
                    assert_eq!(cs.len(), 2);
                    assert_eq!(cs[0].path(), Path::new("more-content"));
                    assert_eq!(cs[1].path(), Path::new("more-content"));
                },
                //_ => panic!("invalid unstaging reason: {:?}", reason),
            }
        },
        _ => panic!("invalid integration result: {:?}", iresult.topic()),
    }

    match *result.old_topic.as_ref().unwrap() {
        OldTopicRemoval::Obsoleted { ref old_merge, ref replacement } => {
            assert_eq!(old_merge,
                       &StagedTopic {
                           merge: CommitId::new(ADD_CONTENT_MERGE),
                           topic: new_topic(ADD_CONTENT,
                                            &topic_identity(),
                                            1,
                                            "add-content",
                                            "add-content"),
                       });
            assert!(replacement.is_none());
        },
        _ => panic!("invalid unstaging reason: {:?}", result.old_topic),
    }
}
