// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_workarea::{self, CommitId};

#[derive(Debug, PartialEq, Eq)]
/// Why a commit is not a valid staging branch commit.
///
/// The staging branch format is such that its first-parent history consists solely of two-parent
/// merge commits. It must also have the base commit as an ancestor.
pub enum InvalidCommitReason {
    /// A non-merge commit was found.
    NonMergeCommit,
    /// An octopus merge commit was found.
    OctopusMerge,
    /// The integration branch is not related to the base.
    NotRelated,
    /// The integration branch does not point to a commit.
    NotACommit,
    /// A merge commit has an invalid commit subject.
    InvalidSubject(String),
    /// A merge commit is missing an ID.
    MissingId,
    /// A merge commit is missing a URL.
    MissingUrl,
    /// A topic has in ID of `0`, which is reserved for the base branch.
    ZeroId,
}

impl InvalidCommitReason {
    /// The commit reason as a string.
    fn as_str(&self) -> &'static str {
        match *self {
            InvalidCommitReason::NonMergeCommit => "non-merge commit",
            InvalidCommitReason::OctopusMerge => "octopus merge",
            InvalidCommitReason::NotRelated => "not related",
            InvalidCommitReason::NotACommit => "not a commit",
            InvalidCommitReason::InvalidSubject(_) => "invalid subject",
            InvalidCommitReason::MissingId => "missing id",
            InvalidCommitReason::MissingUrl => "missing url",
            InvalidCommitReason::ZeroId => "invalid id (0)",
        }
    }
}

error_chain! {
    links {
        GitWorkarea(git_workarea::Error, git_workarea::ErrorKind)
            #[doc = "An error from the git-workarea crate."];
    }

    errors {
        /// The stager was given a branch to manage which is not a valid integration branch.
        InvalidIntegrationBranch(commit: CommitId, reason: InvalidCommitReason) {
            display("invalid integration branch: {}: {}", commit, reason.as_str())
        }

        /// The base of the integration branch may not be unstaged.
        CannotUnstageBase {
            display("cannot unstage base")
        }

        /// An error occurred when executing git commands.
        Git(msg: String) {
            display("git error: {}", msg)
        }

        /// An error occurred when parsing a date.
        DateParse {
            display("failed to parse date")
        }

        /// An error occurred when parsing a topic id.
        IdParse {
            display("failed to parse topic ID")
        }
    }
}
